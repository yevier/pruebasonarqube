import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'rxjs/add/operator/map';

export interface Item { name: string; edad: string; }

@Injectable()
export class ConexionService {

  constructor(
    private firestore: AngularFirestore
  ) { }


  create_NewStudent(record) {
    return this.firestore.collection('Students').add(record);
  }

  read_Students() {
    return this.firestore.collection('Students').snapshotChanges();
  }

  update_Student(recordID, record) {
    this.firestore.doc('Students/' + recordID).update(record);
  }

  // tslint:disable-next-line:variable-name
  delete_Student(record_id) {
    this.firestore.doc('Students/' + record_id).delete();
  }
}
