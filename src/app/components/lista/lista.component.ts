import { Component, OnInit } from '@angular/core';
import { ConexionService } from 'src/app/services/conexion.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.sass']
})
export class ListaComponent implements OnInit {

  title = 'Firestore CRUD Operations Students App';

  students: any;
  studentName: string;
  studentAge: number;
  studentAddress: string;

  constructor(private crudService: ConexionService) { }

  ngOnInit() {
    this.crudService.read_Students().subscribe(data => {

      this.students = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          // tslint:disable-next-line:no-string-literal
          Name: e.payload.doc.data()['Name'],
          // tslint:disable-next-line:no-string-literal
          Age: e.payload.doc.data()['Age'],
          // tslint:disable-next-line:no-string-literal
          Address: e.payload.doc.data()['Address'],
        };
      });
      console.log(this.students);

    });
  }

  CreateRecord() {
    const record = {};
    // tslint:disable-next-line:no-string-literal
    record['Name'] = this.studentName;
    // tslint:disable-next-line:no-string-literal
    record['Age'] = this.studentAge;
    // tslint:disable-next-line:no-string-literal
    record['Address'] = this.studentAddress;
    this.crudService.create_NewStudent(record).then(resp => {
      this.studentName = '';
      this.studentAge = undefined;
      this.studentAddress = '';
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  RemoveRecord(rowID) {
    this.crudService.delete_Student(rowID);
  }

  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditAge = record.Age;
    record.EditAddress = record.Address;
  }

  UpdateRecord(recordRow) {
    const record = {};
    // tslint:disable-next-line:no-string-literal
    record['Name'] = recordRow.EditName;
    // tslint:disable-next-line:no-string-literal
    record['Age'] = recordRow.EditAge;
    // tslint:disable-next-line:no-string-literal
    record['Address'] = recordRow.EditAddress;
    this.crudService.update_Student(recordRow.id, record);
    recordRow.isEdit = false;
  }

}
